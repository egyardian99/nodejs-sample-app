FROM node:11.4.0-alpine

 

WORKDIR /app

COPY package.json /app
COPY .env.example /app/.env


RUN yarn install
RUN yarn build

COPY . /app

CMD [“start”]

EXPOSE 80